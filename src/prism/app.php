<?php
	namespace Prism;
	
	use Silex;

	require_once __DIR__ . '/autoloader.php';

	$app = new Silex\Application();
	$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
	$app->register(new Silex\Provider\TwigServiceProvider(), array(
		'twig.path' => __DIR__ . '/views'
	));
	$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
		'db.options' => array(
			'driver' => 'pdo_sqlite',
			'user' => 'prism',
			'password' => 'ac23rr3',
			'path' => __DIR__ . '/data/log.sqlite'
		)
	));

	$app['debug'] = true;

	// frontend
	$app->get('/', 'Prism\Controllers\defaultController::indexAction')
		->bind('homepage');
	$app->get('/{domain}', 'Prism\Controllers\defaultController::domainAction')
		->bind('domain');
	$app->get('/ping/{email}/{urls}', 'Prism\Controllers\requestController::pingAction')
		->bind('ping');

	// setup
	$app->get('/install', 'Prism\Controllers\configController::installAction')
		->bind('install');

	// api
	$app->get('/api/domains', 'Prism\Controllers\apiController::domainsAction')
		->bind('api_domains');
	$app->get('/api/domain/{domain}', 'Prism\Controllers\apiController::domainAction')
		->bind('api_domain');
	$app->get('/api/info', 'Prism\Controllers\apiController::infoAction')
		->bind('api_domains_status');
	$app->get('/api/info/{domain}', 'Prism\Controllers\apiController::infoDomainAction')
		->bind('api_domain_status');

	$app->run();
?>