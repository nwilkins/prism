<?php
    namespace Prism\Controllers;

    use Silex\Application;
    use Prism\Models\responseRepository;
    use Symfony\Component\HttpFoundation\Response;

    class apiController{
        // get a list of the domains tracked in the last x hours
        public function domainsAction(Application $app){
            $resRepo = new responseRepository($app['db']);
            $data = $resRepo->getDomains();
            $json = json_encode($data);
            $response = new Response($json);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }

        // get all domain information for a specific domain
        public function domainAction(Application $app, $domain){
            $resRepo = new responseRepository($app['db']);
            $data = $resRepo->getDomain($domain);
            $json = json_encode($data);
            $response = new Response($json);
            $response->headers->set('Content-type', 'application/json');
            return $response;
        }

        // get information on a domain specifically formated for a "current status" of the domain
        public function infoDomainAction(Application $app, $domain){
            $resRepo = new responseRepository($app['db']);
            $data = $resRepo->getDomainStatus($domain);
            $json = json_encode($data);
            $response = new Response($json);
            $response->headers->set('Content-type', 'application/json');
            return $response;
        }
    }

?>