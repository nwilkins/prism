<?php
    namespace Prism\Controllers;

    use Silex\Application;
    use Prism\Models\prismRepository;
    use Symfony\Component\HttpFoundation\Response;

    class configController {
        public function installAction(Application $app){
            $repo = new prismRepository($app['db']);
            $repo->init();
            return new Response('Database has been setup');
        }
    }
?>