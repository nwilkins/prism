<?php
	namespace Prism\Controllers;
	
	use Silex\Application;
	use Prism\Models\responseRepository;

	class defaultController{
		public function indexAction(Application $app){
			return $app['twig']->render('default/index.twig');
		}
        public function domainAction(Application $app, $domain){
            return $app['twig']->render('default/single.twig', array('domain' => $domain));
        }
	}
?>