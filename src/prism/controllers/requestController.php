<?php
    namespace Prism\Controllers;

    use Silex\Application;
    use Guzzle\Http\Client;
    use Prism\Models\responseRepository;
    use Symfony\Component\HttpFoundation\Response;

    class requestController{
        public function pingAction(Application $app, $urls, $email){
            $urls = explode(',', $urls);
            $responses = $this->ping($urls);
            
            // log and alert.
            $resRepo = new responseRepository($app['db']);
            foreach($responses as $response){
                $code = $response->getStatusCode();
                $info = $response->getInfo();

                // header alert
                if($code != 200){
                    $this->alertEmail($email, 'code', $response);
                }

                // response time alert
                if($info['total_time'] > 2){
                    $this->alertEmail($email, 'speed', $response);
                }

                // log response
                $resRepo->log($response);
            }

            return new Response();
        }

        private function ping($urls){
            if(count($urls) == 0) return;

            $ping_urls = array();
            $client = new Client();
            $responses = array();
            $verify = array();

            foreach($urls as $url){
                if($url != ''){
                    $request = $client->get('http://' . $url, array(), array('exceptions' => false));
                    $response = $request->send();

                    $info = $response->getInfo();
                    $code = $response->getStatusCode();
                    
                    if($info != 200 || $info['total_time'] > 2){
                        $response = $request->send();
                    }

                    array_push($responses, $response);
                }
            }

            return $responses;
        }

        // private function ping($urls){
        //     if(count($urls) == 0) return;

        //     $ping_urls = array();
        //     $client = new Client();
        //     $verify = array();

        //     foreach($urls as $url){
        //         if($url != ''){
        //             array_push($ping_urls, $client->get('http://' . $url, array(), array('exceptions' => false)));
        //         }
        //     }

        //     $responses = $client->send($ping_urls);

        //     // check responses, if one comes back bad or less than optimal verify.
        //     foreach($responses as $key => $response){
        //         $code = $response->getStatusCode();
        //         $info = $response->getInfo();

        //         // add verification to the bad header and high response time before email and log
        //         if($code != 200 || $info['total_time'] > 2){
        //             unset($responses[$key]);
        //             array_push($verify, $client->get($info['url'], array(), array('exceptions' => false)));
        //         }
        //     }

        //     // if there are urls/domains that need to be verified do so.
        //     if(count($verify) > 0){
        //         $verify_responses = $client->send($verify);
        //         $responses = array_merge($responses, $verify_responses);
        //     }
        //     return $responses;
        // }

        private function alertEmail($email, $type, $response){
            $info = $response->getInfo();
            if($type == "code"){
                mail(
                    $email,
                    'Website Monitor Notification of ' . $response->getStatusCode() . ' Status Code',
                    $info['url'] . ' responded with a ' . $response->getStatusCode() . ' status code - ' . $response->getReasonPhrase() . '.'
                );
            } else if($type == "speed"){
                mail(
                    $email,
                    'Website Monitor Notification of Slow Response Time',
                    $info['url'] . ' took ' . $info['total_time'] * 1000 . 'ms to respond to a request.'
                );
            }
        }
    }

?>