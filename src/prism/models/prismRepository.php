<?php
    namespace Prism\Models;

    use Doctrine\DBAL\Schema\Schema;
    use Doctrine\DBAL\Schema\Comparator;

    class prismRepository{
        public function __construct($db){
            $this->db = $db;
        }

        public function check(){
            // check the db schema should only happen on the class init
            // should never init a repository inside a loop, that would be dumb
            $sm = $this->db->getSchemaManager();
            $diff = $this->getDiff();
            if($diff->toSql($sm->getDatabasePlatform()) == ''){
                return true;
            } else {
                return false;
            }
        }

        public function init(){
            // init the db schema
            $sm = $this->db->getSchemaManager();
            $diff = $this->getDiff();
            $sql = $diff->toSql($sm->getDatabasePlatform());
            foreach($sql as $query){
                $this->db->query($query);
            }
        }

        private function getDiff(){
            $sm = $this->db->getSchemaManager();
            $currentSchema = $sm->createSchema();
            $targetSchema = $this->getSchema();
            $comparator = new Comparator();
            return $comparator->compare($currentSchema, $targetSchema);
        }

        private function getSchema(){
            // set up the default schema
            $schema = new Schema();
            $response = $schema->createTable('response');
            $response->addColumn('id', 'integer', array('unsigned' => true, 'autoincrement' => true));
            $response->addColumn('code', 'integer');
            $response->addColumn('time', 'integer');
            $response->addColumn('domain', 'string', array('notnull' => false));
            $response->addColumn('when', 'datetime', array('default' => 'CURRENT_TIMESTAMP'));
            $response->setPrimaryKey(array('id'));
            $schema->createSequence('response_seq');

            return $schema;
        }
    }
?>