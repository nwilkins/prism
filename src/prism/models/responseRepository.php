<?php
    namespace Prism\Models;

    use Silex\Application;
    use Prism\Models\prismRepository;
    
    class responseRepository extends prismRepository{
        
        // log the connection time, and response code.
        public function log($response){
            $info = $response->getInfo();
            $domain = $this->cleanDomain($info['url']);
            if($domain != ''){
                $stmt = $this->db->prepare('INSERT INTO response (domain, code, time) VALUES (:domain, :code, :time)');
                $stmt->bindValue('domain', $domain);
                $stmt->bindValue('code', $response->getStatusCode());
                $stmt->bindValue('time', intval($info['total_time'] * 1000));
                $stmt->execute();
            }
        }

        // return a list of domains tracked in the last x (default 12) hours
        public function getDomains(){
            $stmt = $this->db->prepare('SELECT DISTINCT domain FROM response WHERE strftime("%s", `when`) > :when');
            $stmt->bindValue('when', time('U') - 43200);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        // returns a domain's set of information for the past x hours (default 12) hours
        public function getDomain($domain){
            $stmt = $this->db->prepare('SELECT domain, code, `when`, time FROM response WHERE domain = :domain AND strftime("%s", `when`) > :when');
            $stmt->bindValue('domain', strval($domain));
            $stmt->bindValue('when', time('U') - 43200);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        // returns a domain's status codes and average for response time for the last hour
        public function getDomainStatus($domain){
            $stmt = $this->db->prepare('SELECT domain, code, AVG(time) FROM response WHERE domain = :domain AND strftime("%s", `when` > :when) ORDER BY `when`');
            $stmt->bindValue('domain', strval($domain));
            $stmt->bindValue('when', time('U') - 3600);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        private function cleanDomain($domain){
           $url = str_replace('www.', '', str_replace('http://', '', str_replace('http://', '', strtolower($domain))));
           $url = explode('/', $url);
           return $url[0];
        }

    }


?>