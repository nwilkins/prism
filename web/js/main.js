var charts = [];
(function($){
    $().ready(function(){
        $.ajax({
            url: '/api/domains',
            success: setup_charts
        });

        setInterval(update_charts, 60000);
    });

    function setup_charts(data){
        for(var i in data){
            $('body').append('<div class="domain domain-' + data[i].domain.replace(/\.|-|_/g, '') + '" data-domain="' + data[i].domain + '"><h5>' + data[i].domain + '</h5><div class="chart"></div></div>');
            $.ajax({
                url: '/api/domain/' + data[i].domain,
                success: add_chart
            });
        }
    }

    function add_chart(data){
        data_set = format_data(data);
        domain = data[0].domain.replace(/\.|-|_/g, '');
        charts[domain] = $.plot('.domain-' + domain + ' .chart', data_set, {
            series: {
                lines: {
                    lineWidth: 1
                }
            },
            grid: {
                borderWidth: 1
            },
            yaxis: {
                min: 0
            },
            xaxis: {
                mode: "time",
                timezone: "browser"
            },
            legend: {
                show: false
            },
            colors: [
                '#0081DE'
            ]
        });
        $('<div class="axisLabel yaxisLabel"></div>').text('Response Time (ms)').appendTo('.domain-' + domain);
        $('<div class="axisLabel xaxisLabel"></div>').text('Time (24 hrs)').appendTo('.domain-' + domain);
    }

    function update_charts(){
        $('.domain').each(function(){
            var domain = $(this).data('domain');
            $.ajax({
                url: '/api/domain/' + domain,
                success: update_chart
            });
        });
    }

    function update_chart(data){
        domain = data[0].domain.replace(/\.|-|_/g, '');
        dt = format_data(data);
        charts[domain].setData(dt);
        charts[domain].setupGrid();
        charts[domain].draw();
    }

    function add_info(data){
        for(var d in data){
            domain = data[d].domain;
            code = data[d].code;
            time = parseInt(data[d]['AVG(time)'], 10);
            $('.info').append('<a href="/' + domain + '" class="btn btn-default">' + domain + ' <span class="label label-primary">' + time + 'ms</span> <span class="label ' + code_to_class(code) + '">' + code + '</span></a>');
        }
    }

    function update_info(data){
        $('.info').html('');
        add_info(data);
    }

    function code_to_class(code){
        if(code == 200){
            return 'label-success';
        }
        return 'label-danger';
    }

    function format_data(data){
        var d = {};
        for(var res in data){
            if(d[data[res].domain] === undefined){
                d[data[res].domain] = {};
                d[data[res].domain].label = data[res].domain;
                d[data[res].domain].data = [];
            }
            var date = new Date(data[res].when.replace(' ', 'T'));
            d[data[res].domain].data.push([parseInt(date.getTime(), 10), parseInt(data[res].time, 10)]);
        }
        var ds = [];
        var keys = Object.keys(d);
        for(var index in keys){
            ds.push(d[keys[index]]);
        }
        return ds;
    }


})(jQuery);
